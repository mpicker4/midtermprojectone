CC = gcc 
CFLAGS = -std=c99 -Wall -Wextra -pedantic -g 

run: project.o menuUtil.o ppmIO.o imageManip.o
	$(CC) -o run project.o menuUtil.o ppmIO.o imageManip.o 

project.o: project.c menuUtil.h ppmIO.h imageManip.h
	$(CC) $(CFLAGS) -c project.c 

menuUtil.o: menuUtil.c menuUtil.h
	$(CC) $(CFLAGS) -c menuUtil.c

ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c

imageManip.o: imageManip.c imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c 

clean:
	rm -f *.o run
