/* File contains all the functions calls that occur in the menu
 * Jpak22@jhu.edu
 * 601.220
 * 10/16/17
 * MidtermProject 1 
 */

#include "menuUtil.h"
#include "ppmIO.h"
#include "imageManip.h"
#include <string.h>
#include <stdlib.h>

	
void printMenu() {
	printf(
	"Main menu:\n"
		"\tr <filename> - read image from <filename> \n"
        "\tw <filename> - write image to <filename> \n"
        "\ts - swap color channels \n"
        "\tbr <amt> - change brightness (up or down) by the given amount \n"
        "\tc <x1> <y1> <x2> <y2> - crop image to the box with the given corners \n"
        "\tg - convert to grayscale \n"
        "\tcn <amt> - change contrast (up or down) by the given amount \n"
        "\tbl <sigma> - Gaussian blur with the given radius (sigma) \n"
        "\tq - quit \n");
}

void run () {

int x1;
int x2; 
int y1;
int y2;
char input[256];
IMAGE *input_img = malloc(sizeof(IMAGE));
	while(1) {
	printMenu();
	   	
	   	while(fgets(input, 256, stdin)!=NULL) {
	   		char v1[100];
	   		char v2[100];
	   		char v3[100];
	   		char v4[100];
	   		char v5[100];
			int n = sscanf(input, "%s %s %s %s %s", v1, v2, v3, v4, v5);

			switch(n) {
			case 1: {
				//make sure that a file was read first 
				if (strcmp(v1, "q")== 0) {
						free(input_img);
						exit(0);
					}
				if (input_img != NULL) {
					if (strcmp(v1, "g")== 0) {
						printf("Going to grayscale\n");
						grayscale(input_img);
					}
					else if (strcmp(v1, "s")== 0) {
						printf("swapping\n");
						swap(input_img);
					}
					break;
				} else {
 					fprintf(stderr, "Error: Make sure you load an image first");
 					goto DEFAULT;
 				}
 			}
			case 2: {
				if (strcmp(v1, "r") == 0) {
					//delete previous read if needed;
					if (input_img == NULL) {
						//free(input_img->pic);
						free(input_img);
					} else {
						printf("reading\n");
						input_img = readImage(v2);
					}
					break;
				}

				else if (input_img != NULL) {
					if (strcmp(v1, "w")== 0) {
						printf("writting\n");
						writeImage(input_img, v2);
						break;
					}
					else if (strcmp(v1, "br")== 0) {
						
						double sigma =atoi(v2);
						if (sigma == 0 && strcmp(v2, "0") != 0) {
							fprintf(stderr, "Error please put in correct brightness adjust\n");
						}
						else {
							printf("adjusting brightness by %f\n", sigma);
							brightness(input_img, sigma);
						}
						break;
					}
					// else if (strcmp(v1, "bl")== 0) {
					// 	double sigma =atof(v2);
					// 	if (sigma > 0) {
					// 		printf("Blurring\n");
					// 		blur(input_img, v2);
					// 	}
					// 	else{
					// 		fprintf(stderr, "Error incorrect blur input\n");
					// 	}
					// }	
					else if (strcmp(v1, "cn") == 0) {
						double amt =atof(v2);
						if (amt > 0) {
							printf("Adjusting Contrast by %f\n", amt);
							contrast(input_img, amt);
						}
						else {
							fprintf(stderr, "Error please input correct contrast levels\n");
						}
						break;
					}
			
				}
				else {
						goto DEFAULT;
					}
			}
			case 5: {
				if (input_img != NULL) {
					if (strcmp(v1, "c") == 0){
						x1 = atoi(v2);
						y1 = atoi(v3);
						x2 = atoi(v4);
						y2 = atoi(v5);	
						printf("cropping coordinates: x1 = %d, y1 = %d, x2 = %d, y2 = %d \n", x1, y1, x2, y2);
						crop(input_img, x1, y1, x2, y2);
						break;
				}
				else 
					goto DEFAULT; 

			}
		
			}
			DEFAULT: {
				printf("Invalid instructions\n");
		}
	}
}
}
}

	
	



			
		

	


