#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <stdio.h>

void crop(IMAGE * image, int x1, int x2, int y1, int y2);
void invert(IMAGE * image);
void swap(IMAGE * image);
void contrast(IMAGE *image, double contrast_amt); 
void brightness(IMAGE *picture_storage, int bright);



#endif
