#ifndef PPMIO_H
#define PPMIO_H

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;   
} pixel;

typedef struct {
    int cols;
    int rows;
    int colors;
    pixel *pic;
    
} IMAGE;

IMAGE * readImage(char * filename);


IMAGE * writeImage(IMAGE *picture_storage, char *filename2);

#endif
