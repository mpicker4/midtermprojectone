/* File contains the functions for loading and writting
 * Ji Pak
 * Jpak22@jhu.edu
 * 601.220
 * 10/16/17
 * MidtermProject 1 
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ppmIO.h"
#include <ctype.h>

IMAGE *readImage(char *filename) {
    IMAGE * picture_storage = malloc(sizeof(IMAGE));
	FILE * f_title;
    f_title = fopen(filename, "r");
    char ch;
    int cols = 0;
    int rows = 0;
    int colors = 0;
    if (!f_title) {
    	fprintf(stderr, "Error no f_title\n");
    	return picture_storage;
    }

    ch = fgetc(f_title);
    if (ch == 'P') {
    	ch = fgetc(f_title);
    	if (ch == '6') {
            while(fgetc(f_title) != '\n') {
            }
    	} else {
    		fprintf(stderr, "Error not a ppm file, 29");
    		return picture_storage;
    	}
	} else {
        fprintf(stderr, "Error not a ppm file 33");
        return picture_storage;
    }

	//while ((ch = fgetc(f_title)) != EOF) {
		//stores the first number as cols
		fscanf(f_title, "%d %d\n", &cols, &rows);
        fscanf(f_title, "%d\n", &colors);
        printf("%d %d %d", colors, cols, rows);
        // printf("%d", colors);
		
		//stores next number as rows
		
		 if(colors != 255) {
		 	printf("Error colors must equal 225");
		 	return picture_storage;
		 		}
		
		 //if all three variables have a value it breaks out of while 
		 
	

	 pixel * pic = malloc(sizeof(pixel)*rows*cols);
	 fread(pic,sizeof(IMAGE), rows*cols, f_title);
	 picture_storage->rows = rows;
	 picture_storage->cols = cols;
   	 picture_storage->colors = colors;
   	 picture_storage->pic = pic; 
   	 fclose(f_title);
   	 return picture_storage;	
}

IMAGE *writeImage(IMAGE *picture_storage, char *filename2) {
    if (!filename2){
        fprintf(stderr, "Error");
    }
    FILE *f_title_2;
    f_title_2 = fopen(filename2, "w"); 
    fprintf(f_title_2, "P6\n%d %d\n%d\n", picture_storage->cols, picture_storage->rows, picture_storage->colors);
    int write = fwrite(picture_storage->pic, sizeof(pixel), (picture_storage->rows)*(picture_storage->cols), f_title_2);
    
    if (write != (picture_storage->rows)*(picture_storage->cols)){
        fprintf(stderr, "error");
    }
     
    fclose(f_title_2);
    return picture_storage;
}





