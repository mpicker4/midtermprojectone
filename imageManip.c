/* File contains all image manipulation files along with their headers
 * Ji Pak
 * Jpak22@jhu.edu
 * 601.220
 * 10/16/17
 * MidtermProject 1 
 */
#include "ppmIO.h"
#include "imageManip.h"
#include <stdio.h>
#include <string.h>
unsigned char max(double color) {
	if (color<0) {
		color = 0;
	}
	else if (color > 255) {
		color = 255;
	}
	unsigned char new_color;
	new_color = (unsigned char)color;
	//printf("clamped value = %d\n", new_color);
	return new_color;
}
//crop has a problem where it just returns the same image. I dont know that I am doing wrong. 
void crop(IMAGE *picture_storage, int x1, int y1, int x2, int y2) {
	int width = (x2 - x1) + 1;
	int height = (y2 - y1) + 1;
	int cols = picture_storage -> cols;
	int rows = picture_storage -> rows;
	// int position  = 0;

	IMAGE *new_image = malloc(sizeof(pixel)*width*height);
	//the file segment faulst when I do this. 
	// new_image -> rows = height; 
	// new_image -> cols = width; 

	if (x1 < 0 || x1 > rows || x2 > rows || y1 < 0 || y1> cols || y2 > cols || x1 > x2 || y1 > y2) {
		fprintf(stderr, "Error; invalide crop inputs");
		free(new_image);
		return;
	}
		//printf("in for loop cropping\n");
		for(int i = y1; i < y2; i++) {
			for(int j=x1; j<x2; j++) {
				new_image->pic[(i-y1) * cols - j - x1] = picture_storage->pic[i * cols + j];
			}
		}
		picture_storage->pic = new_image->pic;
	}	
	
void swap(IMAGE *picture_storage) {
	int rows = picture_storage->rows;
	int cols = picture_storage->cols;
	for (int i = 0; i < rows*cols; i++) {
		pixel *tptr = &(picture_storage->pic[i]);

		int nR = ((*tptr).g);
		int nG = ((*tptr).b);
		int nB = ((*tptr).r);

		((*tptr).r) = nR;
		((*tptr).g) = nG;
		((*tptr).b) = nB;
	}
}

void grayscale(IMAGE *picture_storage) {
	double strength; 

	for (int i = 0; i < (picture_storage->cols)*(picture_storage->rows); i++) {
        pixel *tptr = &(picture_storage->pic[i]);
        strength = 0.30*((*tptr).r) + 0.59*((*tptr).g) + 0.11*((*tptr).b);
		((*tptr).r) = (char)strength;
		((*tptr).g) = (char)strength;
		((*tptr).b) = (char)strength;
    }
}

void brightness(IMAGE *picture_storage, int bright) {
	for (int i = 0; i < ((picture_storage->cols)*(picture_storage->rows)); i++) {
		pixel *tptr = &(picture_storage->pic[i]);
		unsigned char tR = (unsigned char)((*tptr).r);
		unsigned char tG = (unsigned char)((*tptr).g);
		unsigned char tB = (unsigned char)((*tptr).b);
		((*tptr).r) = (unsigned char)max(tR * bright);
    	((*tptr).g) = (unsigned char)max(tG * bright);
    	((*tptr).b) = (unsigned char)max(tB * bright);
	}
}

void contrast(IMAGE *picture_storage, double contrast_amt) {
//	double inc = 1.0/255.0;
	for (int i = 0; i < ((picture_storage->cols)*(picture_storage->rows)); i++) {
		pixel *tptr = &(picture_storage->pic[i]);
		double tR = (double)((*tptr).r);
		double tG = (double)((*tptr).g);
		double tB = (double)((*tptr).b);

		tR = (tR/255)-.5;
		tG = (tG/255)-.5;
		tB = (tB/255)-.5;

		tR = tR*contrast_amt;
		tG = tG*contrast_amt;
		tB = tB*contrast_amt;

		tR = (tR+.5)*255.0;
		tG = (tG+.5)*255.0;
		tB = (tB+.5)*255.0;

		((*tptr).r) = max(tR);
		((*tptr).g) = max(tG);
		((*tptr).b) = max(tB);
	}	
}

// void blur (IMAGE *picture_storage, double blur_sigma) {

// }

// void gauss(int) {
	
//}



