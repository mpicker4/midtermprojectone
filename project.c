/* File runs main code;
 * Ji Pak
 * Jpak22@jhu.edu
 * 601.220
 * 10/16/17
 * MidtermProject 1 
 */
#include <stdio.h>
#include <stdlib.h>
#include "menuUtil.h"

int main () {
	run();
	return 0;
}
